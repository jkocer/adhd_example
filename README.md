## This is a running github help document. Additional information will be added as seen fit

## Clone a repository

1. Locate the folder you want to clone the repository into
     - `git clone https://[username]@bitbucket.org/bobfourney/adhd_uart_s20.git`
	
## Branching fun

1. Create a new branch 
	- Start on branch you want to branch from 
	- This cannot be pushed until a change has been made
    - `git checkout -b [branch_name]`

2. Checkout existing branch
	- `git checkout [branch_name]`
	
## How to commit and push to github

1. Check to see which files have been changed
	- `git status`

2. Add files you wish to commit
	- `git add [filename]`
	- If you wish to add all files
	- `git add *`

3. Create the commit 
	- `git commit -m "[Insert your message]"`
	
4. Push to bit bucket
	- `git push`
	- Sometimes additional commands must be added (especially after creating a new branch) i.e.
	- `git push --set-upstream origin [new_branch_name]`


## Reset current changes

1. Only do this if you want to remove current changes
	- `git reset --hard`
	
